<?php
/**
 * Created by PhpStorm.
 * User: joridos
 * Date: 25/04/16
 * Time: 16:45
 */ 
class BSeller_CepSearch_Model_Resource_Cep extends Mage_Core_Model_Resource_Db_Abstract
{

    protected function _construct()
    {
        $this->_init('bseller_cepsearch/bseller_cepsearch', 'entity_id');
    }

}