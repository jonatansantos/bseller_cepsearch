<?php
class BSeller_CepSearch_Model_ViacepSearch extends Mage_Core_Model_Abstract
{
    private $url = 'https://viacep.com.br/ws/{{cep}}/json/';

    public function loadByCep($cep)
    {
        try{
            $url = str_replace('{{cep}}', $cep, $this->url);
            $client = new Zend_Http_Client(
                $url,
                array(
                    'timeout' => 15,
                    'maxredirects' => 0,
                )
            );

            if($client->request()->getStatus() == 200) {
                $jsonData = $client->request()->getBody();
                $data = Zend_Json::decode($jsonData);
                $cep = str_replace('-', '', $data['cep']);
                $cepSearch = Mage::getModel('bseller_cepsearch/cep')->loadByCep($cep);

                if($cepSearch == NULL && $cep != '') {

                    $cepSearch->setData(array(
                        'cep' => $cep,
                        'address' => $data['logradouro'],
                        'district' => $data['bairro'],
                        'city' => $data['localidade'],
                        'state'=> $data['uf'],
                        'status' => 1
                    ));

                    $cepSearch->save();
                    
                    return $cepSearch;
                }
            }

        }catch (Exception $e){
            Mage::log($e->getMessage(), null, 'BSeller_CepSearch.log', true);
        }

        return NULL;
    }
}