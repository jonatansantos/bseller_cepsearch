<?php
class BSeller_CepSearch_Model_CepSearch extends BSeller_CepSearch_Model_AbstractCepSearch
{
    private $api = NULL;

    function setApi($param = NULL)
    {
        switch ($param) {
            case "viacep":
                $this->api = Mage::getModel('bseller_cepsearch/viacepSearch');
                break;
            case "postmon":
                $this->api = Mage::getModel('bseller_cepsearch/postmonSearch');
                break;
            case "widenet":
                $this->api = Mage::getModel('bseller_cepsearch/widenetSearch');
                break;
            default:
                $this->api = Mage::getModel('bseller_cepsearch/cep');
                break;
        }
        return $this;
    }

    function getCep($cep) {
        
        $cep = $this->api->loadByCep($cep);
        
        return $cep;
    }
}