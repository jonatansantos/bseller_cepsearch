<?php
class BSeller_CepSearch_Model_WidenetSearch extends Mage_Core_Model_Abstract
{
    private $url = 'http://apps.widenet.com.br/busca-cep/api/cep.json';

    public function loadByCep($cep)
    {
        try{
            $client = new Zend_Http_Client(
                $this->url,
                array(
                    'timeout' => 15,
                    'maxredirects' => 0,
                )
            );

            $client->setParameterGet('code', $cep);

            if($client->request()->getStatus() == 200) {
                $jsonData = $client->request()->getBody();
                $data = Zend_Json::decode($jsonData);
                $cep = str_replace('-', '', $data['code']);
                $cepSearch = Mage::getModel('bseller_cepsearch/cep')->loadByCep($cep);

                if($cepSearch == NULL && $cep != '') {

                    $cepSearch->setData(array(
                        'cep' => $cep,
                        'address' => $data['address'],
                        'district' => $data['district'],
                        'city' => $data['city'],
                        'state'=> $data['state'],
                        'status' => 1
                    ));

                    $cepSearch->save();
                    
                    return $cepSearch;
                }
            }

        }catch (Exception $e){
            Mage::log($e->getMessage(), null, 'BSeller_CepSearch.log', true);
        }
        
        return NULL;
    }
}