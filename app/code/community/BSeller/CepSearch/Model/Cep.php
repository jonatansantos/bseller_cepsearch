<?php
/**
 * Created by PhpStorm.
 * User: joridos
 * Date: 25/04/16
 * Time: 16:45
 */ 
class BSeller_CepSearch_Model_Cep extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('bseller_cepsearch/cep');
    }

    public function loadByCep($cep = NULL)
    {
        $this->load($cep, 'cep');
        
        if ($this->isObjectNew()) {
            return NULL;
        }
        
        return $this;
    }
}