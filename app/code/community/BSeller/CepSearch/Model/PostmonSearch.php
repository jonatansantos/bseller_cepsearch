<?php
class BSeller_CepSearch_Model_PostmonSearch extends Mage_Core_Model_Abstract
{
    private $url = 'http://api.postmon.com.br/v1/cep/';

    public function loadByCep($cep)
    {
        try{
            $client = new Zend_Http_Client(
                $this->url . $cep,
                array(
                    'timeout' => 15,
                    'maxredirects' => 0,
                )
            );
            
            if($client->request()->getStatus() == 200) {
                $jsonData = $client->request()->getBody();
                $data = Zend_Json::decode($jsonData);
                $cep = str_replace('-', '', $data['cep']);
                
                //check again to not duplicate cep in database.
                $cepSearch = Mage::getModel('bseller_cepsearch/cep')->loadByCep($cep);
                if($cepSearch == NULL && $cep != '') {
                    $cepSearch = Mage::getModel('bseller_cepsearch/cep');
                    $cepSearch->setData(array(
                        'cep' => $cep,
                        'address' => $data['logradouro'],
                        'district' => $data['bairro'],
                        'city' => $data['cidade'],
                        'state'=> $data['estado'],
                        'status' => 1
                    ));

                    $cepSearch->save();
                    
                    return $cepSearch;
                }
            }
            
        }catch (Exception $e){
            Mage::log($e->getMessage(), null, 'BSeller_CepSearch.log', true);
        }
        
        return NULL;
    }

}