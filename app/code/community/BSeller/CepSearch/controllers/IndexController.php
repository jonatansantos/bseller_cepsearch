<?php
class BSeller_CepSearch_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $cep = $this->getRequest()->getParam('cep');
        $searchFactory = Mage::getModel('bseller_cepsearch/cepSearch');
        $response = Zend_Json::encode(array('status' => 0, 'message' => 'cep not found'));
        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'apxplication/json');

        $fullAddress = $searchFactory->setApi()->getCep($cep);

        if($fullAddress == NULL) {
            $fullAddress = $searchFactory->setApi('postmon')->getCep($cep);
        }

        if($fullAddress == NULL) {
            $fullAddress = $searchFactory->setApi('viacep')->getCep($cep);
        }

        if($fullAddress == NULL) {
            $fullAddress = $searchFactory->setApi('widenet')->getCep($cep);
        }

        if($fullAddress != NULL) {
            $response = Zend_Json::encode(array('status' => 0, 'data' => $fullAddress->getData(), 'message' => 'cep found'));
        }

        echo $response;
    }
}